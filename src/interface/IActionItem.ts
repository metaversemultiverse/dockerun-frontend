export interface IActionItem {
    id: string;
    name: string;
    icon: string;
    enabled: boolean;
    classes: string[];
}

export default IActionItem;
