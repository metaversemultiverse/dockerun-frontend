import DockerConfig from "@/model/DockerConfig";

export default interface IContainer {
    id: string;
    generated: DockerConfig | null;
}
